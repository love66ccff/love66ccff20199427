#include<stdio.h>
#include <stdlib.h>
#include <time.h>
 
void  menu ();
void menu ()
{
	printf("\n操作列表:\n1)一年级    2)二年级    3)三年级\n4)帮助      5)退出程序\n请输入操作> ");
}
void help ();
void help ()
{
	printf("帮助信息\n您需要输入命令代号来进行操作, 且\n一年级题目为不超过十位的加减法;\n二年级题目为不超过百位的乘除法;\n三年级题目为不超过百位的加减乘除混合题目.\n");
}
void error ();
void error ()
{	
	printf("Error!!!\n错误操作指令, 请重新输入\n");
}
void one ();
void one () 
{
  	int i, n,x;
  	scanf("%d",&n);
  	char a;
   	time_t t;
   	srand((unsigned) time(&t));
   for( i = 0 ; i < n ; i++ )
		{
		x=rand()%2;
		if(x == 0)
		{a = '+' ;}
		if(x == 1)
		{a = '-' ;}
      	printf("%d %c %d = __\n", rand() % 10,a,rand() % 10);
      	}
}
void two ();
void two ()
{
	int i, n,x;
  	char a;
    scanf("%d",&n);
   	time_t t;
   	srand((unsigned) time(&t));
   for( i = 0 ; i < n ; i++ )
		{
		x=rand()%2;
		if(x == 0)
		{a = '/' ;}
		if(x == 1)
		{a = '*' ;}
      	printf("%d %c %d = __\n", rand() % 100,a,rand() % 100);
      	}
}
void three ();
void three ()
{
	int i, n,x;
  	char a;
   	scanf("%d",&n);
   	time_t t;
   	srand((unsigned) time(&t));
   for( i = 0 ; i < n ; i++ )
		{
		x=rand()%4;
		if(x == 0)
		{a = '/' ;}
		if(x == 1)
		{a = '*' ;}
		if(x == 2)
		{a = '+';}
		if(x == 3)
		{a = '-';}
      	printf("%d %c %d %c %d = __\n", rand() % 100, a ,rand() % 100, a ,rand() % 100);
      	}
}
int main()
{
	printf("========== 口算生成器 ==========\n欢迎使用口算生成器 :");
	printf("\n\n");
	help ();
	printf("\n");
	menu ();
	int n;
	while(scanf("%d",&n))
	{
		switch (n)
		{
			case 1:printf("现在是一年级的题目:\n请输入生成个数 ");one();break;
			case 2:printf("现在是二年级的题目:\n请输入生成个数 ");two();break;
			case 3:printf("现在是三年级的题目:\n请输入生成个数 ");three();break;
			case 4:help();break;
			case 5:goto end;break;
			default :error();break;
		}
		menu();
	}
	
	end:
	
	return 0;
}
